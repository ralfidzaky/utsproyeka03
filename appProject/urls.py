"""appProject URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include, re_path
from main.views import home as homePage
from formcaridonor.views import *
from ListUDD.views import *
from faq.views import *
from cari_donor.views import *
from django.conf import settings #add this
from django.conf.urls.static import static #add this

urlpatterns = [
    path('admin/', admin.site.urls),
    path('helloWorld/', include('helloWorld.urls')),
    path('home/', include('main.urls')),
    path('FormCariDonor/', include('formcaridonor.urls')),
    path('informasiUDD/', include('ListUDD.urls')),
    path('faq/', include ('faq.urls')),
    path('pendonor/', include ('pendonor.urls')),
    path('cari-donor/', include ('cari_donor.urls')),
    re_path(r'^$', homePage, name='homePage')
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

